eval "$(conda shell.bash hook)" # I really have no idea what this does, but it seemed to make it work.
conda create -y -n n10k python=3.7
source activate n10k
conda install -y -c anaconda scikit-learn=0.22.1
conda install  -y numpy
conda install -y -c conda-forge keras
conda install -y  matplotlib
conda install -y seaborn
conda install -y -c conda-forge jupyterlab
conda install scikit-image
