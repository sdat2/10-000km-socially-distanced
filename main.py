import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from keras.preprocessing.image import load_img, img_to_array, array_to_img
from sklearn.neighbors import BallTree
from utilities.timeit import timeit


@timeit
def get_NZ_data():
    """
    All preproccessing occurs in this step
    """

    CloudTop = np.load("X_train_CI20.npy")
    TrueColor = np.load("Y_train_CI20.npy")
    # TrueColor.shape, CloudTop.shape

    # Preprocessing

    TrueColorNZ = np.delete(TrueColor,
                            np.where(np.sum(TrueColor,
                                            axis=(1, 2, 3)) == 0.), 0)
    CloudTopNZ = np.delete(CloudTop,
                           np.where(np.sum(TrueColor,
                                           axis=(1, 2, 3)) == 0.), 0)

    del TrueColor, CloudTop

    return TrueColorNZ, CloudTopNZ


@timeit
def baseline_solution():

    TrueColorNZ, CloudTopNZ = get_NZ_data()

    MM = CloudTopNZ.reshape((CloudTopNZ.shape[0], -1))
    tree = BallTree(MM, leaf_size = 1)  # changed from 2 ## Is this where fitting occurs?

    # load test set

    X_test = np.load("X_test_CI20_phase1.npy")
    dictAnalogs = {}
    predictions = np.zeros(X_test.shape)

    for i in range(X_test.shape[0]):
        dist, ind = tree.query(X_test[i, ...].reshape((1, 127*127*3)), k=3)
        dictAnalogs[i] = ind
        print(i, dist, ind)
        predictions[i, :] = (TrueColorNZ[ind[0], :, :, :]).mean(0)

    np.save('Y_test_CI20_phase1.npy', predictions)

    test_jessenia(TrueColorNZ, predictions)

    #  maria_method(CloudTopNZ, TrueColorNZ)

# mv 'Y_test_CI20_phase1.npy' 'Y_test_CI20_phase1.predict'
# mv Y_test_CI20_phase1.npy Y_test_CI20_phase1.predict


@timeit
def test_jessenia(TrueColorNZ_test, predictions_1):
    """
    :param TrueColorNZ_test: preprocessed to remove zeros.
    :param predictions_1: y values given the model used.
    :return:
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from skimage import data, img_as_float
    from skimage.metrics import structural_similarity as ssim
    from skimage.metrics import mean_squared_error
    sum_ssim = 0 # my code
    ssim_1 = {}
    mse_none = {}

    for i in range(predictions_1.shape[0]):
        mse_none[i] = mean_squared_error(TrueColorNZ_test[i], predictions_1[i])

        ssim_1[i] = ssim(TrueColorNZ_test[i], predictions_1[i],
                         data_range=predictions_1[i].max() - predictions_1[i].min(),
                         multichannel=True)

        sum_ssim += ssim_1[i]
        # print(i, ssim_1[i])
    # print(ssim_1)
    print("mean_ssim", sum_ssim/len(ssim_1))


@timeit
def maria_method(CloudTopNZ, TrueColorNZ):
    from numpy import array, hstack, math
    from numpy.random import uniform
    import matplotlib.pyplot as plt
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import mean_squared_error
    from sklearn.ensemble import GradientBoostingRegressor
    from sklearn.multioutput import MultiOutputRegressor

    # xtrain, xtest, ytrain, ytest=train_test_split(X, Y, test_size=0.01)

    gbr = GradientBoostingRegressor()
    model = MultiOutputRegressor(estimator=gbr)
    print(model)
    # print("xtest:", xtest.shape, "ytest:", ytest.shape)
    xtrain = CloudTopNZ.reshape((CloudTopNZ.shape[0], -1))
    ytrain = TrueColorNZ.reshape((TrueColorNZ.shape[0], -1))
    print("xtrain:", xtrain.shape, "ytrain:", ytrain.shape)
    model.fit(xtrain, ytrain)
    score = model.score(xtrain, ytrain)
    print("Training score:", score)


@timeit
def maria_method_two():

    from sklearn.datasets import make_regression
    from sklearn.tree import DecisionTreeRegressor

    TrueColorNZ, CloudTopNZ = get_NZ_data()

    # X = CloudTopNZ.reshape((CloudTopNZ.shape[0], -1))
    # y = TrueColorNZ.reshape((TrueColorNZ.shape[0], -1))

    # define model
    model = DecisionTreeRegressor(criterion="friedman_mse")
    # fit model
    # model.fit(X, y)

    X = CloudTopNZ.reshape((CloudTopNZ.shape[0]*127*127, 3))
    y = TrueColorNZ.reshape((TrueColorNZ.shape[0]*127*127, 3))

    model.fit(X, y)

    CloudTopTest = np.load("X_test_CI20_phase1.npy")

    X_test = CloudTopTest.reshape((CloudTopTest.shape[0]*127*127, 3))

    y_test = model.predict(X_test)

    y_train_test = model.predict(X)

    y_train_test = y_train_test.reshape((CloudTopNZ.shape[0], 127, 127, 3))

    predictions = y_test.reshape((200, 127, 127, 3))

    np.save('Y_test_CI20_phase1.npy', predictions)

    test_jessenia(TrueColorNZ, y_train_test) # predictions)


maria_method_two()
# baseline_solution()


import os
os.system("mv Y_test_CI20_phase1.npy Y_test_CI20_phase1.predict")
os.system("zip submission.zip Y_test_CI20_phase1.predict")
